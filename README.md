# ProgressStatusBar

#### 项目介绍
- 项目名称：ProgressStatusBar
- 所属系列：openharmony的第三方组件适配移植
- 功能：系统状态栏的进度视图，显示在系统状态栏的进度条。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 1.2.4

#### 效果演示

![效果演示](./img/showtoastoreo.gif)

#### 安装教程

1、在项目根目录下的build.gradle文件中
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```
2、在entry模块的build.gradle文件中
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:ProgressStatusBar:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

使用该库非常简单，只需查看提供的示例的源代码。

```
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
xmlns:ohos="http://schemas.huawei.com/res/ohos"
ohos:height="match_parent"
ohos:width="match_parent"
ohos:background_element="#3f519b"
ohos:orientation="horizontal">

<com.basel.ProgressStatusBar.ProgressStatusBar
    ohos:progress_color="#40212121"
    ohos:background_instruct_element="#3f519b"
    ohos:id="$+id:progress_status"
    ohos:height="match_content"
    ohos:width="match_parent"/>

</DirectionalLayout>
```

```
 public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initSystemUi();

        ProgressStatusBar progressStatusBar = (ProgressStatusBar) findComponentById(ResourceTable.Id_progress_status);
        progressStatusBar.setProgressWidth(progressStatusBar.getStatusBarHeight(getContext()));
        Button button = (Button) findComponentById(ResourceTable.Id_btn_fake);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                progressStatusBar.startFakeProgress(3000);
            }
        });
        Button handled = (Button) findComponentById(ResourceTable.Id_handled);
        handled.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (curentProgress < 100){
                    curentProgress = curentProgress + 10;
                } else {
                    curentProgress = 0;
                }
                progressStatusBar.setProgress(curentProgress);
            }
        });

        progressStatusBar.setProgressListener(new ProgressStatusBar.OnProgressListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onUpdate(int progress) {
                progressStatusBar.setProgressValue(progress);
            }

            @Override
            public void onEnd() {
                progressStatusBar.setProgressValue(0);
            }
        });
    }

    private void initSystemUi() {
        //状态栏设置为透明
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        //导航栏 ActionBar
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_NAVIGATION);
    }
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原项目组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息

  Apache 2.0

