package com.basel.ProgressStatusBar;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

public class ProgressStatusBar extends ProgressBar implements Component.DrawTask, Component.EstimateSizeListener {

    private Paint progressPaint;
    private int progress;
    private boolean isViewAdded;
    private OnProgressListener pListener;

    /**
     * ProgressStatusBar
     *
     * @param context 上下文
     */
    public ProgressStatusBar(Context context) {
        super(context);
        init();
    }

    /**
     * ProgressStatusBar
     *
     * @param context 上下文
     * @param attrSet AttrSet
     */
    public ProgressStatusBar(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    /**
     * ProgressStatusBar
     *
     * @param context   上下文
     * @param attrSet   attrSet
     * @param styleName styleName
     */
    public ProgressStatusBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        progressPaint = new Paint();
        progressPaint.setStyle(Paint.Style.FILL_STYLE);

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));
        setBackground(shapeElement);
    }

    private void remove() {
        this.progress = 0;
        if (isViewAdded) {
            isViewAdded = false;
        }
    }

    private void prepare() {
        if (!isViewAdded) {
            this.progress = 0;
            isViewAdded = true;
        }
    }

    /**
     * startFakeProgress
     *
     * @param duration 毫秒
     */
    public void startFakeProgress(int duration) {
        prepare();
        AnimatorValue barProgress = new AnimatorValue();
        barProgress.setDuration(duration);

        barProgress.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                progress = (int) (v * 100);
                if (isViewAdded) {
                    invalidate();
                    if (pListener != null) pListener.onUpdate(progress);
                }
            }
        });

        barProgress.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                if (pListener != null) pListener.onStart();
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                if (pListener != null) pListener.onEnd();
                remove();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        barProgress.start();
    }

    /**
     * setProgress
     *
     * @param progress 进度值
     */
    public void setProgress(int progress) {
        prepare();
        if (this.progress == 0 && pListener != null) pListener.onStart();
        this.progress = progress;
        if (progress < 100) {
            invalidate();
            if (pListener != null) pListener.onUpdate(progress);
        } else {
            remove();
            if (pListener != null) pListener.onEnd();
        }
    }

    /**
     * setProgressColor
     *
     * @param color 颜色
     */
    public void setProgressColor(Color color) {
        progressPaint.setColor(color);
    }

    /**
     * setProgressBackgroundColor
     *
     * @param color 颜色
     */
    public void setProgressBackgroundColor(Element color) {
        this.setBackground(color);
    }

    /**
     * onDraw
     *
     * @param component component
     * @param canvas    canvas
     */
    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (progress != 100) {
            int progressEndX = (int) (getWidth() * progress / 100f);
            canvas.drawRect(0, getTop(), progressEndX, getBottom(), progressPaint);
        }
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int width = Component.EstimateSpec.getSize(widthEstimateConfig);
        int height = Component.EstimateSpec.getSize(heightEstimateConfig);
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.NOT_EXCEED),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.NOT_EXCEED));
        return true;
    }

    public interface OnProgressListener {
        /**
         * onStart
         */
        void onStart();

        /**
         * onUpdate
         *
         * @param progress 进度
         */
        void onUpdate(int progress);

        /**
         * onEnd
         */
        void onEnd();
    }

    public void setProgressListener(OnProgressListener progressListener) {
        pListener = progressListener;
    }

    /**
     * 状态栏高度
     *
     * @param context 上下文
     * @return int
     */
    public int getStatusBarHeight(Context context) {
        int statusHeight = (int) (getRealHeight(context) - getHeight(context));
        return statusHeight;
    }

    private float getHeight(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getSize(point);
        return point.position[1];
    }

    private float getRealHeight(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getRealSize(point);
        return point.position[1];
    }

}
