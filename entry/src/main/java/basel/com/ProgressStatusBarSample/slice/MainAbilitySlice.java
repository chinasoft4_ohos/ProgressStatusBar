package basel.com.ProgressStatusBarSample.slice;

import basel.com.ProgressStatusBarSample.ResourceTable;
import com.basel.ProgressStatusBar.ProgressStatusBar;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.WindowManager;

public class MainAbilitySlice extends AbilitySlice {

    private int curentProgress = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initSystemUi();

        ProgressStatusBar progressStatusBar = (ProgressStatusBar) findComponentById(ResourceTable.Id_progress_status);
        progressStatusBar.setProgressWidth(progressStatusBar.getStatusBarHeight(getContext()));
        int width = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes().width;
        progressStatusBar.setWidth(width + 50);
        Button button = (Button) findComponentById(ResourceTable.Id_btn_fake);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                progressStatusBar.startFakeProgress(3000);
            }
        });
        Button handled = (Button) findComponentById(ResourceTable.Id_handled);
        handled.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (curentProgress < 100){
                    curentProgress = curentProgress + 10;
                } else {
                    curentProgress = 0;
                }
                progressStatusBar.setProgress(curentProgress);
            }
        });

        progressStatusBar.setProgressListener(new ProgressStatusBar.OnProgressListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onUpdate(int progress) {
                progressStatusBar.setProgressValue(progress);
            }

            @Override
            public void onEnd() {
                progressStatusBar.setProgressValue(0);
            }
        });
    }

    private void initSystemUi() {
        //状态栏设置为透明
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        //导航栏 ActionBar
        //getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_NAVIGATION);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
